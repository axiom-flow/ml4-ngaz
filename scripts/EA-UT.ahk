﻿r := A_NowUTC
r -= 19700101000000, s
EPOCH := (r * 1000 + A_MSec) "`n" GetSystemTimeAsUnixTime()
idx := 0

GetSystemTimeAsUnixTime() {
   static UNIX_TIME_START := 0x019DB1DED53E8000  ; January 1, 1970 (start of Unix epoch) in "ticks"
        , TICKS_PER_SECOND := 10000000  ; A tick is 100ns
   
   DllCall("GetSystemTimeAsFileTime", "Int64*", UTC_Ticks)  ; Returns ticks in UTC
   Return (UTC_Ticks - UNIX_TIME_START) / TICKS_PER_SECOND
}

path := "C:\MT4\unittesting\UT-" . A_DD . "-" . A_MM . "-" . A_YYYY
FileCreateDir, %path%

Run "C:\Program Files\FXCM\terminal.exe",,, NewPID
Process, Priority, %NewPID%, High
Sleep 6000
Send, !{F4}
Sleep 1000
Send, !{F4}
Sleep 1000

idx := (%idx%+1)
file := path . "\" . idx . "-" . EPOCH
Screenshot(file)
Sleep 1000

MouseClick, left, 132, 128, 1, 50
Sleep 1000
MouseClick, left, 86, 154, 1, 50
Sleep 1000
MouseClick, left, 1485, 158, 1, 50
Sleep 1000
MouseClick, left, 178, 502, 1, 50
Sleep 1000
MouseClick, left, 1740, 160, 1, 50
Sleep 1000
MouseClick, left, 1705, 214, 1, 50
Sleep 5000

return

Screenshot(absolutepath)
{
    doScreenshot: ;<- a callable label
	Send, +{PrintScreen} ;<- grabs the desktop puts on clipboard
	Run mspaint.exe,,, NewScreenPID
	Process, Priority, %NewScreenPID%, High
	WinWaitActive, Paint ;<- wait for paint to be active
	Send, ^v ;<- paste the image into paint
	Sleep 1000 ;<- let it settle
	Send {F12} ;<- activate save dialog
	Sleep 1000 ;<- let it settle
	Send, %absolutepath% ;<- name the file as temp.png
	Sleep 1000 ;<- let it settle
	Send, {Enter} ;<- activate the save
	sleep 1000 ;<- let it settle
	Process, Close, %NewScreenPID%
	sleep 1000
}