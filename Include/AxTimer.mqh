//+------------------------------------------------------------------+
//|                                                      AxTimer.mqh |
//|                        Copyright 2021, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2021, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

#include <AxLog.mqh>

class AxTimer
{
   private:
      ulong m8u_current_timing;
      ulong m8u_last_timing;
      ulong m8u_schdTiming;
      string mz_tmpLine;
      
      AxLog mo_Log;
      string mz_moduleName;
      short m2_logHandle;
      
   public:
     /**
      * AxTimer default constructor
      */
      AxTimer():m8u_schdTiming(ulong(PeriodSeconds(Period()))), 
                m8u_last_timing(Time[0]), 
                mo_Log("AXTIMER", 0){};
    
     /**
      * AxTimer default constructor
      * @param p8r_timing const ulong reference, value is wakeup timing
      * @see sample : if your current period is 30 minutes and you want a wakeup each 30min : mytimer = AxTimer(ulong(PeriodSeconds(Period())));
      */
      AxTimer(const ulong & p8r_timing):m8u_schdTiming((p8r_timing <= 0) ? ulong(PeriodSeconds(Period())) : p8r_timing), 
                                        m8u_last_timing(Time[0]), 
                                        mo_Log("AXTIMER", 0){};
      
     /**
      * chk_timer wakeup call check : true wakeup syscall
      * @return true : wakeup call
      */
      bool chk_timer();
      
     /**
      * write a string in the current AXTIMER log ctx
      */
      void write(string pzr_str);
      
     /**
      * closing the logfile (code of conduct)
      */
      void end_timer();
};

bool AxTimer::chk_timer()
{
   if((Time[0] - m8u_last_timing) >= m8u_schdTiming)
   {
      mz_tmpLine = "current T : " + IntegerToString(Time[0]) + ", m8u_last_timing : " + IntegerToString(m8u_last_timing) + ", m8u_schdTiming : " + IntegerToString(m8u_schdTiming);
      mo_Log.writeString(mz_tmpLine);
      
      m8u_last_timing = Time[0];
      return true;
   }
   return false;
}

void AxTimer::write(string pzr_str)
{
   mo_Log.writeString(pzr_str);
}

void AxTimer::end_timer()
{
   mo_Log.writeString("EOF");
}

//+------------------------------------------------------------------+
