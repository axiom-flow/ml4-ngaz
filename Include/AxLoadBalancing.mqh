//+------------------------------------------------------------------+
//|                                              AxLoadBalancing.mqh |
//|                        Copyright 2021, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2021, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

class AxLoadBalancing
{
   private:
      uint m4u_maxLoad;
      uint m4u_currentLoad;
      
   public:
      /**
      * AxLoadBalancing ctr
      * @param p4r_load const uint reference, max amount of stocks authorized
      */
      AxLoadBalancing(const uint & p4r_load):m4u_maxLoad(p4r_load),m4u_currentLoad(0){};
      
      /**
      * lockLoad reserve a specific amount of stocks
      * @param p4r_load const uint reference, amount to reserve
      * @return true : if stocks amount reserved
      */
      bool lockLoad(const uint & p4r_load);
      
      /**
      * freeLoad unlock a specific amount of stocks
      * @param p4r_load const uint reference, amount to unload
      * @return true : if stocks amount unloaded
      */
      bool freeLoad(const uint & p4r_load);
};

bool AxLoadBalancing::lockLoad(const uint & p4r_load)
{
   if(m4u_currentLoad + p4r_load > m4u_maxLoad)
      return false;
      
   m4u_currentLoad += p4r_load;
   return true;
}

bool AxLoadBalancing::freeLoad(const uint & p4r_load)
{
   if(p4r_load > m4u_currentLoad)
      return false;
      
   m4u_currentLoad -= p4r_load;
   return true;
}

//+------------------------------------------------------------------+
