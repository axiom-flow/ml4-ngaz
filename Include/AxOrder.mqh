//+------------------------------------------------------------------+
//|                                                      AxOrder.mqh |
//|                        Copyright 2021, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2021, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

#include <AxLoadBalancing.mqh>

class AxOrder
{
   private:
      AxLoadBalancing mo_LoadBalancing;
      
   public:
      /**
      * AxOrder ctr
      * @param p4r_maxLoad const uint reference, max amount of stocks authorized
      */
      AxOrder(const uint & p4r_maxLoad):mo_LoadBalancing(p4r_maxLoad){};
};

//+------------------------------------------------------------------+
