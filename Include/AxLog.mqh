//+------------------------------------------------------------------+
//|                                                        AxLog.mqh |
//|                        Copyright 2021, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2021, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

class AxLog
{
   private:
      string maz_fileName;
      string maz_insertStamp;
      
      /**
       * rebuildStamp method operate on the rebuild of the file stamp
       * @return void
       */
      void rebuildStamp()
      {
         maz_insertStamp = TimeToStr(TimeCurrent(), TIME_SECONDS|TIME_DATE);
         StringReplace(maz_insertStamp, ".", "-");
         StringReplace(maz_insertStamp, " ", "|");
         maz_insertStamp = "[" + maz_insertStamp + "] ";
      }
      
   public:
      
      /**
       * writeString parameters
       * @param pzk_newLine const string defining the new line to add in the current AxLog file.
       * @return int [0 -> ok; 1 -> pzk_newLine error; 2 -> FileOpen error; 3 -> FileIsExist false]
       */
      int writeString(const string pzk_newLine)
      {
         if(StringLen(pzk_newLine) <= 0)
            return 1;
         
         if(FileIsExist(maz_fileName, 0))
         {
            int l4_fileHandle = FileOpen(maz_fileName, FILE_READ|FILE_WRITE|FILE_TXT);
            
            if(l4_fileHandle != INVALID_HANDLE)
            {
               rebuildStamp();
               
               FileSeek(l4_fileHandle, 0, SEEK_END);
               FileWriteString(l4_fileHandle, maz_insertStamp + pzk_newLine + "\n");
               FileClose(l4_fileHandle);
               
               return 0;
            }
            
            return 2;
         }
         
         return 3;
      }
   
      /**
       * AxLog parameters constructor
       * @param pzk_moduleName const string defining the file name.
       * @param p2k_conflictHandle const short argument defining the way we handle conflicts during two same file name.
       * @return The test results
       */
      AxLog(string pz_moduleName, short p2_conflictHandle)
      {
         if(StringLen(pz_moduleName) <= 0)
            pz_moduleName = "UNKNOWN";
         
         if(p2_conflictHandle != 0 && 
            p2_conflictHandle != 1 && 
            p2_conflictHandle != 2)
            p2_conflictHandle = 0;
         
        /** 
         * maz_fileName file name definition.
         * [MODULE_NAME][-][RANDTICK][-][DATETIME][.TXT]
         */
         MathSrand(GetTickCount());
         maz_fileName = pz_moduleName + "-" + IntegerToString((((MathRand() * Time[0]) / 100) % 100)) + "-" + TimeToStr(TimeCurrent(), TIME_SECONDS|TIME_DATE) + ".txt";
         
         StringReplace(maz_fileName, " ", "_");
         StringReplace(maz_fileName, ":", ".");
         
        /** 
         * 0 (legacy : wait new [EPOCH]) 
         * 1 (soft : moving conflict one as backtrace [BACKLOG_MODULE_NAME])
         * 2 (hard : deleting conflict)
         */
         if(FileIsExist(maz_fileName, 0))
         {
            switch(p2_conflictHandle)
            {
               case 0:
               {
                  for(int itt = 0; FileIsExist(maz_fileName, 0); ++itt)
                  {
                     maz_fileName = pz_moduleName + 
                                    "-" +
                                    IntegerToString((((MathRand() * itt) / 1000) % 1000)) + 
                                    "-" + 
                                    TimeToStr(TimeCurrent(), TIME_SECONDS|TIME_DATE) + 
                                    ".txt";
                                    
                     StringReplace(maz_fileName, " ", "_");
                     StringReplace(maz_fileName, ":", ".");
                  }
               }
               break;
               
               case 1:
               {
                  string lz_backLogName = "BACKLOG_" + maz_fileName;
                  if(FileMove(maz_fileName, 0, lz_backLogName, FILE_COMMON|FILE_REWRITE))
                     Alert("File moved as a backlog trace");
                  else
                     Alert("Error! Code = " + DoubleToString(GetLastError()));
               }
               break;
               
               case 2:
               {
                  while(FileIsExist(maz_fileName, 0))
                     FileDelete(maz_fileName);
               }
               break;
               
               default:
                  Alert("Wrong p2k_conflictHandle");
            }
         }
         
         maz_insertStamp = TimeToStr(TimeCurrent(), TIME_SECONDS|TIME_DATE);
         StringReplace(maz_insertStamp, ".", "-");
         StringReplace(maz_insertStamp, " ", "|");
         maz_insertStamp = "[" + maz_insertStamp + "] ";
         
         int l4_fileHandle = FileOpen(maz_fileName , FILE_READ|FILE_WRITE|FILE_TXT);
         FileWriteString(l4_fileHandle, maz_insertStamp + "Writing to Newly Created File Completed\n");
         
         Alert("New log file created : " + maz_insertStamp);
         
         FileClose(l4_fileHandle);
      };
      
      ~AxLog(){};
};

//+------------------------------------------------------------------+
