//+------------------------------------------------------------------+
//|                                                  AxIndicator.mqh |
//|                        Copyright 2021, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+

#property copyright "Copyright 2021, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

class AxIndicator
{
   private:
      double TRIX[];
      double VWAP;
      double KStoOsc;
      
   public:
      double getVWAP(){return VWAP;};
      double getTRIX(const int & p4r_period);
      
      uint assertCall(const uint & p4r_period);
      
      void computeVWAP(const uint & p4r_period);
      void computeTRIX(const uint & p4r_period);
      
      void computeStochasticOscillator(const uint & p4r_period);
      
      AxIndicator();
      ~AxIndicator();
};

uint AxIndicator::assertCall(const uint & p4r_period)
{
   if(!(p4r_period > 0))
      return -1;
   
   switch(p4r_period)
   {
      case 0:
         return p4r_period;
      break;
      case 1:
         return p4r_period;
      break;
      default:
         return 10;
      break;
   }
}

double AxIndicator::getTRIX(const int & p4r_period)
{
   if(p4r_period <= ArraySize(TRIX))
      return TRIX[p4r_period];

   return -1;
}

void AxIndicator::computeVWAP(const uint & p4r_period)
{
   double PV = ((High[p4r_period] + Low[p4r_period] + Close[p4r_period]) / 3) * Volume[p4r_period];
   VWAP = PV / Volume[p4r_period];
}

void AxIndicator::computeTRIX(const uint & p4r_period)
{
   //wrong call
   if(assertCall(p4r_period) > 0)
   {
      uint EMA_period = p4r_period;
      uint SMA_period = EMA_period * 2;
      
      uint itt = SMA_period + EMA_period;
      double SMA_twenty = 0;
      double EMA[];
   
      ArrayFree(TRIX);   
      ArrayResize(EMA, p4r_period, 0);
      ArrayResize(TRIX, p4r_period, 0);
      
      //divide the period by 2 since SMA period is 20 days before EMA period
      //SMA interval is 40 to 20
      //EMA interval is 20 to 0
      double EMA_multiplier = 2 / (EMA_period + 1);
         
      //SMA computing from 40 to 20
      for(; itt > SMA_period; SMA_twenty += Close[itt--]);
      SMA_twenty /= SMA_period;
       
      //EMA period must be computed with SMA as the yesterday value
      EMA[itt] = Close[itt] * EMA_multiplier + SMA_twenty * EMA_multiplier;
         
      //EMA computing
      for(; itt > 0; --itt, EMA[itt] = Close[itt] * EMA_multiplier + EMA[itt + 1] * EMA_multiplier);
         
      //TRIX computing
      for(itt = p4r_period; itt > 0; TRIX[itt] = (EMA[itt] - EMA[itt + 1]) / EMA[itt + 1], itt--);
   }
}

void AxIndicator::computeStochasticOscillator(const uint & p4r_period)
{
   if(assertCall(p4r_period) >= 0)
   {
      double LowestPrevious = Close[p4r_period], HighestPrevious = Close[p4r_period];
      
      //fetch the lowest and the highest price traded during the same 14 day period
      for(uint itt = p4r_period; itt < p4r_period + 14; ++itt)
      {
         if(Close[itt] > HighestPrevious)
         {
            HighestPrevious = Close[itt];
         }
         
         if(Close[itt] < LowestPrevious)
         {
            LowestPrevious = Close[itt];
         }
      }
      
      KStoOsc = ((Close[p4r_period] - LowestPrevious) / (HighestPrevious - LowestPrevious)) * 100;
   }
}
