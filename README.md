**Expert Advisor**

[[_TOC_]]

## EA - architectural model

```mermaid
graph TB

  subgraph "EA interdependence"
  Node1[EA_insiderGUI] --> Node2[AxFSM]
  style Node2 fill:#9cf,stroke:#f66,stroke-width:2px,color:#fff,stroke-dasharray: 5 5
  Node2[AxFSM] --> SubGraph1[AxOrder]
  Node2[AxFSM] --> Node3[AxIndicators]
  Node2[AxFSM] --> Node12[Logs]
  style Node12 fill:#fe7,stroke:#f66,stroke-width:2px
  Node3[AxIndicators] -- bind indicators --> Node4[linkage]
  Node4[linkage] -- fetch group results --> Node3[AxIndicators]
  Node1[EA_insiderGUI] --> Node5[AxScheduler]
  style Node5 fill:#9cf,stroke:#f66,stroke-width:2px,color:#fff,stroke-dasharray: 5 5
  Node5[AxScheduler] --> Node6[Logs]
  style Node6 fill:#fe7,stroke:#f66,stroke-width:2px
  Node5[AxScheduler] --> SubGraph2[AxExecutionModel]

  SubGraph1 --> SubGraph1Flow
  subgraph "Order manager"
  SubGraph1Flow(AxOrder)
  SubGraph1Flow -- position risk tracing --> AxRiskManager
  SubGraph1Flow --> Node9[AxLoadBalancing]
  end

  SubGraph2 --> SubGraph2Flow
  subgraph "Execution configuration"
  SubGraph2Flow(AxExecutionModel)
  SubGraph2Flow --> Node10[AxTimer]
  end

end
```

## EA - how it work

### AxScheduler ###

> The scheduler main task is the orchestration of one or more FSM, it main task is to manage concurrent or sequential FSM run(s)

The scheduler can provide different type of execution model based on the termFSM definition.
Each execution result in a load, which is managed on two variables : the throughput of the load, the fairness of the run.

The analytic part is defined on two level :
- on the execution model efficiency (response time)
- on the scheduler in a logfile tracing all the action decided at each time

{- Above all, the scheduler should be able to deliver a preemptive or a cooperative process between multiple scheduler. Meaning that multiple GUI would be attach to a pool of scheduler for a cooperative process. On the other hand we'll have a 'master' scheduler which'll instanciate 'slave(s)' scheduler for a preemptive one. -}

---

#### AxExecutionModel ####

> This component is used as a master node clas of AxTimer, in which, AxExecutionModel define N amounts of timers related to N amounts of actions, where actions equals preemptive FSM runs.

The execution model will be the handle of AxTimer linked list, with predefined actions given for AxScheduler, here's the containers for more understanding :

`struct ex_model {
  AxTimer _axtimer;
  AxFSM _axfsm;
};`

`extern int amt_models=30;`

`ex_model _axExecutionModel_i[amt_models];`

##### AxTimer #####

> It's an abstraction of the internal server clock, which handle predefined wake up calls.

It's main role is to trigger wake up calls based on system ticks. There are configuration settings used for optimizing the preemptivness of the AxTimer :
- m8u_schdTiming : argument to define the wake up trigger frequency, by default set to `ulong(PeriodSeconds(Period()))`

```mermaid
graph TB

  subgraph "AxTimer workflow"
  Node1[AxTimer] -- defining m8u_schdTiming --> Node2[OnTick]
  Node2[OnTick] -- is m8u_schdTiming overpassed --> Node3[chk_timer]
  Node3[chk_timer] -- TRUE --> Node4[wakeup]
  Node3[chk_timer] -- FALSE --> Node5[sleep]

end
```

### AxFSM ###

> The Finite State Machine in this type of context use different type of input and operate over finate states based on the linkage of multiple indicator in order to provide a risk testing profile

Relating the FSM we'll have two type of profile :
- the one using one indicators
- the other computing multiple indicators through a process called the 'linkage'

Inside the FSM we have a class named AxOrder which is in charge of delivering two operative : 
- determining the risk of an indicator result
- the profit handler managing the buy and sell order and tracing each action in a logfile

#### AxIndicators ####

> This class must be grasped as an external plugin library, holding specific but also experimental trading indicators computions.

It split in two indicators definitions which both contain, the same two classes.

Definitions :
- stable : which should be use for production, with a predefined rate of risk
- experimental : which should stay in a sandbox for testing, or use by caution with a starting risk pinpoint set above the average.

Classes :
- generic : Known, and most used indicators over the trading world.
- specific : Specific property of one nor multiple trading symbols.

### Logs ###
> Logfile will be delivered at the root folder in a file hierarchy following this path : root/logfile/FSM_GUI_xxxx_HH:MM:SS_DD-MM-YYYY_logfile.txt

As for now we'll provide those type of logfiles :
- FSM -> PROFIT HANDLER : `root/logfile/FSM_GUI_PROFITHANDLER_HH:MM:SS_DD-MM-YYYY_logfile.txt`
- SCHEDULER -> LOGS : `root/logfile/SCHEDULER_GUI_LOGS_HH:MM:SS_DD-MM-YYYY_logfile.txt`
- SCHEDULER -> TIMER : `root/logfile/SCHEDULER_GUI_TIMER_HH:MM:SS_DD-MM-YYYY_logfile.txt`

#info
