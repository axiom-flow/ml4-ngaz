//+------------------------------------------------------------------+
//|                                                      PK          |
//|                        Copyright 2021, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2021, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

#include <AxTimer.mqh>
#include <AxOrder.mqh>
#include <AxIndicator.mqh>

AxTimer _Timer;

//Test values
double order;
const int g4k_period = 6;

//A flag defining if the pool was used or not
bool g1_pool_order = false;
//The current volume of open positions
int g4_pool_order_amount = 0;
//The limit amount of open positions
const int g4k_pool_order_limit = 1;

//The initial balance value
double g8_genesis_balance;

//[SMA]
//Simple Moving Average
double compute_SMA(const int & p4r_duration) {
   //Minimal value for duration is 4 days
   if(p4r_duration < 4)
      return -1;

   double l8_sma = 0;
   for(int l4_itt = p4r_duration; l4_itt >= 0; l8_sma += Close[l4_itt--]);
   l8_sma /= p4r_duration;
   
   return l8_sma;
}

//[EMA]
//Closing price is referenced to now
//Exponential Moving Average
double compute_EMA(const int & p4r_duration) {
   //Minimal value for duration is 4 days
   if(p4r_duration < 4)
      return -1;
      
   //Compute the first 'yesterday'
   double l8_genesis_ema = compute_SMA(p4r_duration);
   
   //Computing the smoothing constant
   double l8_smoothing = 2 / (p4r_duration + 1);
   
   //Compute the EMA
   double l8_ema = 0;
   for(int l4_itt = p4r_duration; l4_itt >= 0; --l4_itt) {
      l8_ema = (l4_itt == p4r_duration) ? l8_genesis_ema : l8_ema;
      l8_ema = (Close[l4_itt] - l8_ema) * l8_smoothing + l8_ema;
   }
   return l8_ema;
}

//[EMA]
//Compute the EMA on over a specific 
void compute_EMA_array(int p4r_mode, const int & p4r_duration, double & p8a_ema[]) {

   //Minimal value for duration is 4 days
   if(p4r_duration < 4)
      return;
   
   //Computing the smoothing constant
   double l8_smoothing = double(2.0 / (double(p4r_duration) + 1.0));
   
   double l8a_vbuffer[];
   //legacy EMA run (no EMA array provided as source)
   if(p4r_mode == 0) {
      ArrayCopy(l8a_vbuffer, Close, 0, 0, p4r_duration);
   
      //Compute the first 'yesterday'
      double l8_genesis_ema = compute_SMA(p4r_duration);
      p8a_ema[p4r_duration - 1] = (l8a_vbuffer[p4r_duration - 1] - l8_genesis_ema) * l8_smoothing + l8_genesis_ema;
   }
   //smoothing EMA run (EMA array provided as source for smoothing)
   else {
      ArrayCopy(l8a_vbuffer, p8a_ema, 0, 0, p4r_duration);
   }
   
   //Compute the EMA
   for(int l4_itt = p4r_duration - 2; l4_itt >= 0; --l4_itt) 
   {
      p8a_ema[l4_itt] = (l8a_vbuffer[l4_itt] - p8a_ema[l4_itt + 1]) * l8_smoothing + p8a_ema[l4_itt + 1];
      printf("EMA[" + IntegerToString(p4r_mode + 1) + "][" + IntegerToString(l4_itt) + "] : (" + DoubleToString(l8a_vbuffer[l4_itt]) + " - " + DoubleToString(p8a_ema[l4_itt + 1]) + ") * " + DoubleToString(l8_smoothing) + " + " + DoubleToString(p8a_ema[l4_itt + 1]) + " = " + DoubleToString(p8a_ema[l4_itt]));
   }
}

//[TRIX] 
double compute_TRIX(const int & p4r_duration) {
   //configuration parameters
   double l8_trix = 0;
   
   //triple smoothing
   double l8a_EMA[];
   ArrayResize(l8a_EMA, p4r_duration);
   ArrayInitialize(l8a_EMA, EMPTY_VALUE);
   ArrayFill(l8a_EMA, 0, p4r_duration, 0);
   
   compute_EMA_array(0, p4r_duration, l8a_EMA);
   compute_EMA_array(1, p4r_duration, l8a_EMA);
   compute_EMA_array(2, p4r_duration, l8a_EMA);
   
   for(int l4_itt = p4r_duration - 2; l4_itt >= 0; --l4_itt)
      l8_trix = (l8a_EMA[l4_itt] - l8a_EMA[l4_itt + 1]) * 1000;
   
   return l8_trix;
}

//[SMMA]
//SMoothed Moving Average
double compute_SMMA(const int & p4r_duration) {
   //Minimal value for duration is 4 days
   if(p4r_duration < 4)
      return -1;

   //Compute the first value SMMA is SMA
   double l8_genesis_smma = compute_SMA(p4r_duration);

   //Compute the smma to the current value
   double l8_smma = 0;
   for(int l4_itt = p4r_duration; l4_itt >= 0; --l4_itt) {
      l8_smma = (l4_itt == p4r_duration) ? l8_genesis_smma : l8_smma;
      l8_smma = (l8_smma * (p4r_duration - 1) + Close[l4_itt]) / p4r_duration; 
   }
   return l8_smma;
}

int OnInit() { return(INIT_SUCCEEDED);}
void OnDeinit(const int reason) { _Timer.end_timer(); }

void OnTick()
{
   const int period = 14;
   
   if (_Timer.chk_timer()) {
      _Timer.write(string("Period closing price : " + DoubleToString(Close[0])));
      _Timer.write(string("SMMA(" + IntegerToString(period) + ") : " + DoubleToString(compute_SMMA(period))));
      _Timer.write(string("TRIX(" + IntegerToString(period) + ") : " + DoubleToString(compute_TRIX(period))));
   }
   
   //Save the initial account balance
   if(g1_pool_order == false) {
      g8_genesis_balance = AccountBalance();
   }
   
   double l8_smma = compute_SMMA(g4k_period);
/*
   if((currentPrice > movingAverage) && (currentPrice < lastPrice)){
      order = OrderSend(NULL, OP_SELL, 1, currentPrice, 0, NULL, NULL, NULL, 0, 0, NULL);
   } else if((currentPrice < movingAverage) && (currentPrice > lastPrice)){
      order = OrderSend(NULL, OP_BUY, 1, currentPrice, 0, NULL, NULL, NULL, 0, 0, NULL);
   }   
*/
}
//+------------------------------------------------------------------+


